package com.naviworld.rest.api.emulator;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;
import org.springframework.core.env.Environment;
import org.apache.log4j.Logger;

public class EmulatorToFileLoger {
    
    // Инициализация логера
    private static final Logger log = Logger.getLogger(EmulatorToFileLoger.class);
    
    public void writeRqRsLog (String data, UUID uuid, String dataType, Environment env) {
        String fileName = uuid.toString() + "_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss"));
        try(FileWriter writer = new FileWriter(env.getProperty("loggingRqRsPath") + "/Calculator/" + dataType + "_" + fileName + ".xml", true)) {
            writer.write(data);
            writer.flush();
            writer.close();
        }
        catch(IOException ex){
            log.error(ex.getMessage());
        }
    }
    
    public void writeTimeoutLog (long startTime, LocalDateTime timeStamp, Environment env) {
        long delayTime = System.currentTimeMillis() - startTime;
        try(FileWriter writer = new FileWriter(env.getProperty("loggingTimeoutPath"), true)) {
            String text = String.valueOf(timeStamp) + ";" + delayTime + "\n";
            writer.write(text);
            writer.flush();
            writer.close();
        }
        catch(IOException ex) {
            log.error(ex.getMessage());
        }
    }
}
