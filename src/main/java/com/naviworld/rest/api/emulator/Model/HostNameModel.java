package com.naviworld.rest.api.emulator.Model;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 *
 * @author navi
 */
public class HostNameModel {
    private static String host;
    private static String hostname;
    
    public HostNameModel () throws UnknownHostException {
        InetAddress localHostNetworkAddress = InetAddress.getLocalHost();
        host = localHostNetworkAddress.getHostAddress();
        hostname = localHostNetworkAddress.getHostName();
    }
    
    public String getHost () {
        return host;
    }
    
    public String getHostname () {
        return hostname;
    }
}
