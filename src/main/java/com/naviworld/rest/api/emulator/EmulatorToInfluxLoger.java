package com.naviworld.rest.api.emulator;

import com.naviworld.rest.api.emulator.Model.HostNameModel;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;
import org.influxdb.dto.Query;
import org.springframework.core.env.Environment;

/**
 *
 * @author navi
 */
public class EmulatorToInfluxLoger {
    
    private static final Logger log = Logger.getLogger(EmulatorToInfluxLoger.class);
    
    public void writeRqRsLogToInflux (String appname, String message, String procid, Long timestamp, Environment env) throws UnknownHostException {
        HostNameModel hostNameNetwork = new HostNameModel();
        // about loging params https://en.wikipedia.org/wiki/Syslog
        Point point = Point.measurement("rq_rs_body")
                .time(System.currentTimeMillis() * 1000, TimeUnit.MICROSECONDS)
                .tag("appname", appname)
                .tag("host", hostNameNetwork.getHost())
                .tag("hostname", hostNameNetwork.getHostname())
                .addField("message", message)
                .addField("procid", procid)
                .addField("timestamp", timestamp)
                .build();
        Query getDatabase = new Query("SHOW DATABASES;");
        Query createDatabase = new Query ("CREATE DATABASE " + env.getProperty("influxDb.DBName") + ";");
        try (InfluxDB influxDB = InfluxDBFactory.connect(env.getProperty("iunfluxDb.url"))) {
            if (!influxDB.query(getDatabase).toString().contains(env.getProperty("influxDb.DBName"))){
                influxDB.query(createDatabase);
            }
            influxDB.setDatabase(env.getProperty("influxDb.DBName"));
            influxDB.write(point);
            influxDB.close();
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
    }

    public void writeTimeoutLogToInflux (String appname, String procid, Long timestamp, Environment env) throws UnknownHostException {
        long delayTime = System.currentTimeMillis() - timestamp;
        HostNameModel hostNameNetwork = new HostNameModel();

        // about loging params https://en.wikipedia.org/wiki/Syslog
        Point point = Point.measurement("timeout")
                .time(System.currentTimeMillis() * 1000, TimeUnit.MICROSECONDS)
                .tag("appname", appname)
                .tag("host", hostNameNetwork.getHost())
                .tag("hostname", hostNameNetwork.getHostname())
                .addField("message", delayTime)
                .addField("procid", procid)
                .addField("timestamp", timestamp)
                .build();
        Query getDatabase = new Query("SHOW DATABASES;");
        Query createDatabase = new Query ("CREATE DATABASE \"" + env.getProperty("influxDb.DBName") + "\"");
        try (InfluxDB influxDB = InfluxDBFactory.connect(env.getProperty("iunfluxDb.url"))) {
            if (!influxDB.query(getDatabase).toString().contains(env.getProperty("influxDb.DBName"))){
                influxDB.query(createDatabase);
            }
            influxDB.setDatabase(env.getProperty("influxDb.DBName"));
            influxDB.write(point);
            influxDB.close();
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
    }
}
