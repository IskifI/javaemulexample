package com.naviworld.rest.api.emulator.Controllers;

//import org.springframework.web.bind.annotation.RequestMapping;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import java.util.UUID;
import java.time.LocalDateTime;
import java.util.concurrent.CompletableFuture;
import java.net.HttpURLConnection;
import java.net.URL;
import com.naviworld.rest.api.emulator.EmulatorToFileLoger;
import com.naviworld.rest.api.emulator.EmulatorToInfluxLoger;
import java.io.IOException;
import java.net.UnknownHostException;

/**
 *
 * @author navi
 */

@RestController
//@RequestMapping("/calculator")
public class CalculatorController {
    
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(CalculatorController.class);
        
    private static final String TEMPLATE = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + 
            "<s:Envelope xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">" + 
            "<s:Body><AdditionResponse xmlns=\"http://tempuri.org/\"><AdditionResult xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">" + 
            "<Result>%s</Result></AdditionResult></AdditionResponse></s:Body></s:Envelope>";
    
    // Получение данных из файла application.properties
    @Autowired
    private Environment env;
    
    @PostMapping("/Calculator.asmx")
    public String calculator (@RequestBody String request) throws InterruptedException, UnknownHostException, IOException {
        LocalDateTime timeStamp = LocalDateTime.now();
        long startTime = System.currentTimeMillis();
        UUID uuid = UUID.randomUUID();
        int A = 0, B = 0;
        String actionSub, action = "";
        
        if (Boolean.parseBoolean(env.getProperty("loggingRqRs"))) {
            String dataType = "request";
            EmulatorToFileLoger loger = new EmulatorToFileLoger ();
            loger.writeRqRsLog (request, uuid, dataType, env);
        }
        if (Boolean.parseBoolean(env.getProperty("useInfluxDBlogingRqRs"))) {
            String appname = "calculator.asmx";
            EmulatorToInfluxLoger logerInflux = new EmulatorToInfluxLoger();
            logerInflux.writeRqRsLogToInflux(appname, request, uuid.toString(), startTime, env);
        }
        
        CompletableFuture sendGetAsyn = CompletableFuture.runAsync(() -> {
            try {
                getMessage(request, startTime, timeStamp, uuid);
            } catch (Exception ex) {
                log.error(ex.getMessage());
            }
        });
        
//        CompletableFuture sendPostAsyn = CompletableFuture.runAsync(() -> {
//            try {
//                postMessage (request, startTime, timeStamp, uuid);
//            } catch (Exception ex) {
//                log.error(ex.getMessage());
//            }
//        });
        
        try {
            A = Integer.parseInt(request.substring(request.indexOf("<tem:intA>") + 10, request.indexOf("</tem:intA>")));
            B = Integer.parseInt(request.substring(request.indexOf("<tem:intB>") + 10, request.indexOf("</tem:intB>")));
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
        
        try {
            actionSub = request.substring(request.indexOf("<tem:") + 5);
            action = actionSub.substring(0, actionSub.indexOf(">"));
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }

        int result;
        
        switch (action) {
            case "Addition":
                result = A + B;
                break;
            case "Subtraction":
                result = A - B;
                break;
            case "Multiplication":
                result = A * B;
                break;
            case "Division":
                result = A / B;
                break;
            default:
                log.error("Bad request!");
                result = 101;
        }
        
        String response = String.format(TEMPLATE, String.valueOf(result));
        
        Thread.sleep(Long.parseLong(env.getProperty("timeSleep")));
        
        if (Boolean.parseBoolean(env.getProperty("loggingRqRs"))) {
            String dataType = "response";
            EmulatorToFileLoger loger = new EmulatorToFileLoger ();
            loger.writeRqRsLog (response, uuid, dataType, env);
        }
        if (Boolean.parseBoolean(env.getProperty("useInfluxDBlogingRqRs"))) {
            String appname = "calculator.asmx";
            EmulatorToInfluxLoger logerInflux = new EmulatorToInfluxLoger();
            logerInflux.writeRqRsLogToInflux(appname, response, uuid.toString(), startTime, env);
        }
        
        if (Boolean.parseBoolean(env.getProperty("loggingTimeout"))) {
            EmulatorToFileLoger loger = new EmulatorToFileLoger ();
            loger.writeTimeoutLog (startTime, timeStamp, env);
        }
        if (Boolean.parseBoolean(env.getProperty("useInfluxDBlogingTimeout"))) {
            String appname = "calculator.asmx";
            EmulatorToInfluxLoger logerInflux = new EmulatorToInfluxLoger();
            logerInflux.writeTimeoutLogToInflux(appname, uuid.toString(), startTime, env);
        }
        
        return response;
    }
    
    public void getMessage (String requestBody, long startTime, LocalDateTime timeStamp, UUID uuid) throws Exception {
        try {
            Thread.sleep(2000);
            URL urlObj = new URL("http://www.google.com/search?q=perftest");
            HttpURLConnection connection = (HttpURLConnection) urlObj.openConnection();

            connection.setRequestMethod("GET");
            connection.setRequestProperty("User-Agent", "Mozilla/5.0");
            Integer responseCode = connection.getResponseCode();
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
        
        if (Boolean.parseBoolean(env.getProperty("loggingTimeout"))) {
            EmulatorToFileLoger loger = new EmulatorToFileLoger ();
            loger.writeTimeoutLog (startTime, timeStamp, env);
        }
        if (Boolean.parseBoolean(env.getProperty("useInfluxDBlogingTimeout"))) {
            String appname = "calculator.asmx";
            EmulatorToInfluxLoger logerInflux = new EmulatorToInfluxLoger();
            logerInflux.writeTimeoutLogToInflux(appname, uuid.toString(), startTime, env);
        }
    }
    
    public void postMessage (String request, long startTime, LocalDateTime timeStamp, UUID uuid) throws Exception {
        Thread.sleep(2000);
        URL url = new URL("https://185.177.94.166/Calculator.asmx");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        String responceBody = "";
        connection.setRequestMethod("POST");

        StringBuilder postData = new StringBuilder();
        postData.append(TEMPLATE);

        byte[] postDataBytes = postData.toString().getBytes("UTF-8");
        connection.setDoOutput(true);
        try (DataOutputStream writer = new DataOutputStream(connection.getOutputStream())) {
            writer.write(postDataBytes);
            writer.flush();
            writer.close();

            StringBuilder content;

            try (BufferedReader in = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()))) {
            String line;
            content = new StringBuilder();
               while ((line = in.readLine()) != null) {
                    content.append(line);
                    content.append(System.lineSeparator());
                }
            } catch (Exception ex) {
                log.error(ex.getMessage());
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
        } finally {
            connection.disconnect();
        }
        
        if (Boolean.parseBoolean(env.getProperty("loggingRqRs"))) {
            String dataType = "responce";
            EmulatorToFileLoger loger = new EmulatorToFileLoger ();
            loger.writeRqRsLog (responceBody, uuid, dataType, env);
        }
        if (Boolean.parseBoolean(env.getProperty("useInfluxDBlogingRqRs"))) {
            String appname = "calculator.asmx";
            EmulatorToInfluxLoger logerInflux = new EmulatorToInfluxLoger();
            logerInflux.writeRqRsLogToInflux(appname, postData.toString(), uuid.toString(), startTime, env);
        }
        
        if (Boolean.parseBoolean(env.getProperty("loggingTimeout"))) {
            EmulatorToFileLoger loger = new EmulatorToFileLoger ();
            loger.writeTimeoutLog (startTime, timeStamp, env);
        }
        if (Boolean.parseBoolean(env.getProperty("useInfluxDBlogingTimeout"))) {
            String appname = "calculator.asmx";
            EmulatorToInfluxLoger logerInflux = new EmulatorToInfluxLoger();
            logerInflux.writeTimeoutLogToInflux(appname, uuid.toString(), startTime, env);
        }
    }
}